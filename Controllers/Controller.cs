using Microsoft.AspNetCore.Mvc;
using System;

namespace REST.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class Controller : ControllerBase
    {
        [HttpGet]
        public String Get()
        {
            return "Hello World!";
        }

        [HttpPost]
        [ActionName("hello")]
        public String Post(String name)
        {
            return "Hello " + name + "!";
        }
    }

}